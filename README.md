# Sécuriser Wordpress grâce à .htaccess

## Introduction

Il y a deux manières de sécuriser son site Wordpress :
- au niveau du site, via le fichier functions.php (voir [ici](https://gitlab.com/sakura-lain/my-wordpress-theme))
- au niveau du serveur, via le ou les fichier(s) .htaccess

## Options utiles de .htaccess

### Limiter l'accès à l'espace administrateur à certaines IP
```
AuthUserFile /dev/null
AuthGroupFile /dev/null
AuthName "WordPress Admin Access Control"
AuthType Basic
<LIMIT GET>
order deny,allow
deny from all
# whitelist Syed's IP address
allow from xx.xx.xx.xxx
# whitelist David's IP address
allow from xx.xx.xx.xxx
</LIMIT
```

### Protéger le dossier administrateur avec un mot de passe 
Associer ce code à un fichier .htpasswd :
```
AuthName "Admins Only"
AuthUserFile /home/yourdirectory/.htpasswds/public_html/wp-admin/passwd
AuthGroupFile /dev/null
AuthType basic
require user putyourusernamehere
<Files admin-ajax.php>
Order allow,deny
Allow from all
Satisfy any 
</Files>
```

### Désactiver la navigation dans les répertoires
```
Options -Indexes
```

### Bloquer l'énumération des utilisateurs
```
# Block User ID Phishing Requests
<IfModule mod_rewrite.c>
	RewriteCond %{QUERY_STRING} ^author=([0-9]*)
	RewriteRule .* http://example.com/? [L,R=302]
</IfModule>
```

### Désactiver l'exacution de code php dans les dossiers wp-includes et uploads

Pour ce faire, placer dans ces deux dossier un fichier .htaccess contenant les lignes suivantes :
```
<Files *.php>
deny from all
</Files>
```

### Protéger le fichier wp-config.php
```
<files wp-config.php>
order allow,deny
deny from all
</files>
```

### Paramétrer des redirections 301
```
Redirect 301 /oldurl/ http://www.example.com/newurl
Redirect 301 /category/television/ http://www.example.com/category/tv/
```

### Bannir des IP suspectes
```
<Limit GET POST>
order allow,deny
deny from xxx.xxx.xx.x
allow from all
</Limit>
```

### Empêcher le hotlinking
```
#disable hotlinking of images with forbidden or custom image option
RewriteEngine on
RewriteCond %{HTTP_REFERER} !^$
RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?wpbeginner.com [NC]
RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?google.com [NC]
RewriteRule \.(jpg|jpeg|png|gif)$ – [NC,F,L]
```

### Protéger le fichier .htaccess lui-même des accès non autorisés
```
<files ~ "^.*\.([Hh][Tt][Aa])">
order allow,deny
deny from all
satisfy all
</files>
```

### Augmenter la taille d'upload autorisée
```
php_value upload_max_filesize 64M
php_value post_max_size 64M
php_value max_execution_time 300
php_value max_input_time 300
```

### Désactiver l'accès à XML-RPC
```
# Block WordPress xmlrpc.php requests
<Files xmlrpc.php>
order deny,allow
deny from all
</Files>
```

### Empêcher le scan des auteurs
```
# BEGIN block author scans
RewriteEngine On
RewriteBase /
RewriteCond %{QUERY_STRING} (author=\d+) [NC]
RewriteRule .* - [F]
# END block author scans
```

## Sources concernant .htaccess

- https://perishablepress.com/stop-user-enumeration-wordpress/
- https://www.thesitewizard.com/blogging/block-author-id-archive-urls-with-htaccess.shtml

## Générateur de fichier .htpasswd
- https://hostingcanada.org/htpasswd-generator/

## Autres sources concernant la sécurité de WordPress et son déboggage

- https://news.infomaniak.com/securiser-son-site-wordpress/
- https://audreytips.com/conseils-securiser-wordpress/
- https://www.codeur.com/blog/securiser-site-wordpress/
- https://www.codeur.com/tuto/wordpress/proteger-wordpress-attaques/#4_masquer_la_version_de_wordpress
- https://fr.wordpress.org/support/article/debugging-in-wordpress/
- https://security.szurek.pl/en/prevent-wpscan-from-scanning.html
